package ru.tsc.felofyanov.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.tsc.felofyanov.tm")
public class ApplicationConfiguration {
}
