package ru.tsc.felofyanov.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.tsc.felofyanov.tm.model.Project;

import java.util.List;

@RequestMapping("/api/projects")
public interface ProjectRestEndpoint {

    @GetMapping("/count")
    long count();

    @PostMapping("/delete")
    void delete(@RequestBody Project project);

    @PostMapping("/deleteAll")
    void clear(@RequestBody List<Project> projects);

    @DeleteMapping("/clear")
    void clear();

    @DeleteMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @GetMapping("/findAll")
    List<Project> findAll();

    @GetMapping("/findById/{id}")
    Project findById(@PathVariable("id") String id);

    @PostMapping("/save")
    Project save(@RequestBody Project project);
}
