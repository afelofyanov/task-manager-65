package ru.tsc.felofyanov.tm.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.tsc.felofyanov.tm.api.ProjectRestEndpoint;
import ru.tsc.felofyanov.tm.model.Project;

import java.util.Arrays;
import java.util.List;

public class ProjectRestEndpointClient implements ProjectRestEndpoint {

    private static final String ROOT = "http://localhost:8080/api/projects/";

    public static void main(String[] args) {
        final ProjectRestEndpoint client = new ProjectRestEndpointClient();
        final List<Project> projects = client.findAll();
        System.out.println("Количество проектов: "+ client.count());

        for(final Project project: projects) {
            System.out.println("Проект: " + project.getName() +"; ID: "+ project.getId());
        }

        final String id = projects.get(0).getId();
        System.out.println("Существование проекта: " + client.existsById(id));

        final Project project = client.findById(id);
        System.out.println("Поиск проекта: "+ project.getName());

        client.deleteById(id);
        System.out.println("Поиск проекта после удаления по id: "+ client.existsById(id));
        System.out.println("Количество проектов: "+ client.count());

        client.clear();
        System.out.println("Количество проектов после удаления всех: "+ client.count());

        client.save(project);
        System.out.println("Количество проектов после сохранения: "+ client.count());

        client.delete(project);
        System.out.println("Количество проектов после удаления: "+ client.count());

        client.save(project);
        System.out.println("Сохранили: "+ client.count());

        client.clear(projects);
        System.out.println("Количество проектов после удаления: "+ client.count());
    }

    @Override
    public long count() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "count";
        return restTemplate.getForObject(ROOT + url, Long.class);
    }

    @Override
    public void delete(Project project) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "delete";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity entity = new HttpEntity(project, headers);
        restTemplate.postForObject(ROOT + url, entity, Project.class);
    }

    @Override
    public void clear(List<Project> projects) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "deleteAll";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<List<Project>> entity = new HttpEntity<>(projects, headers);
        restTemplate.postForObject(ROOT + url, entity, Project[].class);
    }

    @Override
    public void clear() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "clear";
        restTemplate.delete(ROOT + url, Project.class);
    }

    @Override
    public void deleteById(String id) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "deleteById/{id}";
        restTemplate.delete(ROOT + url, id);
    }

    @Override
    public boolean existsById(String id) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "existsById/{id}";
        return restTemplate.getForObject(ROOT + url, Boolean.class, id);
    }

    @Override
    public List<Project> findAll() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "findAll";
        final Project[] result = restTemplate.getForObject(ROOT + url, Project[].class);
        return Arrays.asList(result);
    }

    @Override
    public Project findById(String id) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "findById/{id}";
        return restTemplate.getForObject(ROOT + url, Project.class, id);
    }

    @Override
    public Project save(Project project) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "save";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity entity = new HttpEntity(project, headers);
        return restTemplate.postForObject(ROOT + url, entity, Project.class);
    }
}
