package ru.tsc.felofyanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.felofyanov.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProjectRepository {

    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("Test"));
        add(new Project("One"));
        add(new Project("Two"));
    }

    public void create() {
        add(new Project("New project: " + System.currentTimeMillis()));
    }

    public Project add(@NotNull final Project project) {
        return projects.put(project.getId(), project);
    }

    public Project save(@NotNull final Project project) {
        return projects.replace(project.getId(), project);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(String id) {
        projects.remove(id);
    }

    public void remove(@NotNull final Project project) {
        projects.remove(project.getId());
    }

    public void remove(@NotNull final List<Project> projects) {
        projects.stream().forEach(this::remove);
    }

    public boolean existsById(@NotNull final String id) {
        return projects.containsKey(id);
    }

    public void clear() {
        projects.clear();
    }

    public long count() {
        return projects.size();
    }
}
