package ru.tsc.felofyanov.tm.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.felofyanov.tm.api.TaskRestEndpoint;
import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.repository.TaskRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpointImpl implements TaskRestEndpoint {

    @Autowired
    private TaskRepository repository;

    @Override
    @GetMapping("/count")
    public long count() {
        return repository.count();
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody Task task) {
        repository.remove(task);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear(@RequestBody List<Task> tasks) {
        repository.remove(tasks);
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        repository.clear();
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") String id) {
        repository.removeById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") String id) {
        return repository.existsById(id);
    }

    @Override
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return repository.findAll().stream().collect(Collectors.toList());
    }

    @Override
    @GetMapping("/findById/{id}")
    public Task findById(@PathVariable("id") String id) {
        return repository.findById(id);
    }

    @Override
    @PostMapping("/save")
    public Task save(@RequestBody Task task) {
        return repository.save(task);
    }
}
