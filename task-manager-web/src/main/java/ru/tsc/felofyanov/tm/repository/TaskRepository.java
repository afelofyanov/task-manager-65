package ru.tsc.felofyanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.felofyanov.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TaskRepository {

    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("Task1"));
        add(new Task("Task2"));
        add(new Task("Task3"));
    }

    public void create() {
        add(new Task("New Task: " + System.currentTimeMillis()));
    }

    public void add(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public Task save(@NotNull final Task task) {
        return tasks.replace(task.getId(), task);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

    public void remove(@NotNull final Task project) {
        tasks.remove(project.getId());
    }

    public void remove(@NotNull final List<Task> tasks) {
        tasks.stream().forEach(this::remove);
    }

    public boolean existsById(@NotNull final String id) {
        return tasks.containsKey(id);
    }

    public void clear() {
        tasks.clear();
    }

    public long count() {
        return tasks.size();
    }
}
