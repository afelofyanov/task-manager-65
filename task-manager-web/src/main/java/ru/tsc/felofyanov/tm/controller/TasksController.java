package ru.tsc.felofyanov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.felofyanov.tm.repository.TaskRepository;

@Controller
public class TasksController {

    @Autowired
    private TaskRepository repository;

    @GetMapping("/tasks")
    public ModelAndView index() {
        return new ModelAndView("task-list", "tasks", repository.findAll());
    }
}
