package ru.tsc.felofyanov.tm.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.tsc.felofyanov.tm.api.TaskRestEndpoint;
import ru.tsc.felofyanov.tm.model.Task;

import java.util.Arrays;
import java.util.List;

public class TaskRestEndpointClient implements TaskRestEndpoint {

    private static final String ROOT = "http://localhost:8080/api/tasks/";

    public static void main(String[] args) {
        final TaskRestEndpoint client = new TaskRestEndpointClient();
        final List<Task> tasks = client.findAll();
        System.out.println("Количество проектов: "+ client.count());

        for(final Task task: tasks) {
            System.out.println("Проект: " + task.getName() +"; ID: "+ task.getId());
        }

        final String id = tasks.get(0).getId();
        System.out.println("Существование проекта: " + client.existsById(id));

        final Task task = client.findById(id);
        System.out.println("Поиск проекта: "+ task.getName());

        client.deleteById(id);
        System.out.println("Поиск проекта после удаления по id: "+ client.existsById(id));
        System.out.println("Количество проектов: "+ client.count());

        client.clear();
        System.out.println("Количество проектов после удаления всех: "+ client.count());

        client.save(task);
        System.out.println("Количество проектов после сохранения: "+ client.count());

        client.delete(task);
        System.out.println("Количество проектов после удаления: "+ client.count());

        client.save(task);
        System.out.println("Сохранили: "+ client.count());

        client.clear(tasks);
        System.out.println("Количество проектов после удаления: "+ client.count());
    }

    @Override
    public long count() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "count";
        return restTemplate.getForObject(ROOT + url, Long.class);
    }

    @Override
    public void delete(Task task) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "delete";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity entity = new HttpEntity(task, headers);
        restTemplate.postForObject(ROOT + url, entity, Task.class);
    }

    @Override
    public void clear(List<Task> tasks) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "clear";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<List<Task>> entity = new HttpEntity<>(tasks, headers);
        restTemplate.postForObject(ROOT + url, entity, Task.class);
    }

    @Override
    public void clear() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "clear";
        restTemplate.delete(ROOT + url, Task.class);
    }

    @Override
    public void deleteById(String id) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "deleteById/{id}";
        restTemplate.delete(ROOT + url, id);
    }

    @Override
    public boolean existsById(String id) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "existById/{id}";
        return restTemplate.getForObject(ROOT + url, Boolean.class, id);
    }

    @Override
    public List<Task> findAll() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "findAll";
        final Task result = restTemplate.getForObject(ROOT + url, Task.class);
        return Arrays.asList(result);
    }

    @Override
    public Task findById(String id) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "findById/{id}";
        return restTemplate.getForObject(ROOT + url, Task.class, id);
    }

    @Override
    public Task save(Task task) {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "save";
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity entity = new HttpEntity(task, headers);
        return restTemplate.postForObject(ROOT + url, entity, Task.class);
    }
}
