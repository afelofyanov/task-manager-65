package ru.tsc.felofyanov.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.tsc.felofyanov.tm.model.Task;

import java.util.List;

@RequestMapping("/api/tasks")
public interface TaskRestEndpoint {

    @GetMapping("/count")
    long count();

    @PostMapping("/delete")
    void delete(@RequestBody Task task);

    @PostMapping("/deleteAll")
    void clear(@RequestBody List<Task> tasks);

    @DeleteMapping("/clear")
    void clear();

    @DeleteMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @GetMapping("/findAll")
    List<Task> findAll();

    @GetMapping("/findById/{id}")
    Task findById(@PathVariable("id") String id);

    @PostMapping("/save")
    Task save(@RequestBody Task task);
}
