package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataLoadBase64Request extends AbstractUserRequest {

    public DataLoadBase64Request(@Nullable String token) {
        super(token);
    }
}
