package ru.tsc.felofyanov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.felofyanov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.felofyanov.tm.api.service.IAuthService;
import ru.tsc.felofyanov.tm.api.service.dto.IUserDTOService;
import ru.tsc.felofyanov.tm.dto.model.SessionDTO;
import ru.tsc.felofyanov.tm.dto.model.UserDTO;
import ru.tsc.felofyanov.tm.dto.request.UserLoginRequest;
import ru.tsc.felofyanov.tm.dto.request.UserLogoutRequest;
import ru.tsc.felofyanov.tm.dto.request.UserProfileRequest;
import ru.tsc.felofyanov.tm.dto.response.UserLoginResponse;
import ru.tsc.felofyanov.tm.dto.response.UserLogoutResponse;
import ru.tsc.felofyanov.tm.dto.response.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Getter
@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.felofyanov.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    @NotNull
    @Autowired
    private IUserDTOService userService;

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserLoginRequest request
    ) {
        @NotNull final IAuthService authService = getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserLogoutRequest request
    ) {
        check(request);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserProfileRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @NotNull final UserDTO user = getUserService().findOneById(session.getUserId());
        return new UserProfileResponse(user);
    }
}
