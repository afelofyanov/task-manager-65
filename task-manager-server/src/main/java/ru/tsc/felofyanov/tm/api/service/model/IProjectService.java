package ru.tsc.felofyanov.tm.api.service.model;

import ru.tsc.felofyanov.tm.model.Project;

public interface IProjectService extends IUserOwnerService<Project> {

}
