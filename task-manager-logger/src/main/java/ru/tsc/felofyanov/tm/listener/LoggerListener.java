package ru.tsc.felofyanov.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.ILoggerService;
import ru.tsc.felofyanov.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class LoggerListener implements MessageListener {

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        if (!(message instanceof TextMessage)) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        loggerService.writeLog(textMessage.getText());
    }
}
